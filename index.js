

const http = require('http');

const port = 3000;

http.createServer(function(request, response) {

	if(request.url == '/login'){

	response.writeHead(200,{'Content-Type': 'text/plain'});
	response.end('You are in the login page');


} else {
	
		response.writeHead(404,{'Content-Type': 'text/plain'});
		response.end('Sorry the page you are looking for cannot be found' );

}

}).listen(port);

console.log('server is successfully running');
