// a. what directive is used by node.js in loading the modules it needs?

// require()

//b what node.js module contains a method for server creation?

// HTTP


//c. what is the method of the http object responsible for creating a server using node.js

// createServer()

// d. what method of the response object allows us to set status codes and content types?

// writeHead()

// e. where will the console.log() output its contents when run in node.js?

// terminal

//f. what property of the request object contains the address endpoint?

// any url such as /homepage or /register or /login